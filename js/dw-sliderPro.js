jQuery(document).ready(function ($) {

	$('#dw-slider').sliderPro({
		responsive: true,
		width: '100%',
		height: 500,
		arrows: true,
		fadeArrows: true,
		buttons: false,
		autoSlideSize: true,
		centerImage: true,
		thumbnailArrows: false,
		thumbnailHeight: 100,
		thumbnailPointer: true,
		imageScaleMode: 'exact',
		autoplay: true,
		startSlide: 0,
		autoplayDelay: 5000,
		breakpoints: {
			800: {
				thumbnailsPosition: 'bottom',
				thumbnailWidth: 270,
				thumbnailHeight: 100,
				height: 450
			},
			650: {
				thumbnailsPosition: 'bottom',
				thumbnailWidth: 270,
				thumbnailHeight: 100,
				height: 340
			},
			500: {
				thumbnailsPosition: 'bottom',
				thumbnailWidth: 120,
				thumbnailHeight: 50,
				height: 300
			}
		}
	});

});