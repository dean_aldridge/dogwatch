jQuery(document).ready(function ($) {

	$('.dw-toggle-container').first().find('.gb-inside-container').toggleClass('dw-open').find('.dwToggleBody').css('display', 'block');

	$('.dwToggleHeader').click(function () {
		$(this).parents('.gb-inside-container').toggleClass('dw-open').find('.dwToggleBody').slideToggle(250);
	});

});