This sets up the SASS and gulp environment for a generatepress child theme ('dogwatch'). It relies on a the generatepress parent being present.

 GULP
 CLI version 2.3.0
 Local version 4.0.2
 
 NODE v16.15.0
 NPM v 8.8.0

To start an instance:

1. npm init
Sets up package.json, just accept all defaults

2. npm install
Installs npm files

3. gulp
Runs the gulp task which currently builds css -> style.css, sets up a watch for any changes to sc