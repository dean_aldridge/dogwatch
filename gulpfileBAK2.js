const gulp = require('gulp');
const sass = require('gulp-sass');
const del = require('del');
const sourcemaps = require('gulp-sourcemaps');

gulp.task('styles', () => {
    return gulp.src('scss/**/*.scss')
        .pipe(sourcemaps ? sourcemaps.init() : noop())
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps ? sourcemaps.write() : noop())
        .pipe(gulp.dest('./css/'));
});

gulp.task('clean', () => {
    return del([
        'css/dogwatch.css',
    ]);
});

gulp.task('watch', () => {
    gulp.watch('sass/**/*.scss', (done) => {
        gulp.series(['clean', 'styles'])(done);
    });
});

// gulp.task('default', gulp.series(['clean', 'styles']));
