const gulp = require('gulp');
const sass = require('gulp-sass');
// const browserSync = require('browser-sync').create();
const del = require('del');
const sourcemaps = require('gulp-sourcemaps');

gulp.task('styles', () => {
	return gulp.src('scss/dogwatch.scss')
		// .pipe(sourcemaps ? sourcemaps.init() : noop())
		.pipe(sass().on('error', sass.logError))
		// .pipe(sourcemaps ? sourcemaps.write() : noop())
		.pipe(gulp.dest('./'));
});

gulp.task('clean', () => {
	return del([
		'dogwatch.css',
	]);
});

gulp.task('watch', () => {
	gulp.watch('scss/**/*.scss', (done) => {
		gulp.series(['clean', 'styles'])(done);
	});
});

gulp.task('default', gulp.series(['clean', 'styles']));